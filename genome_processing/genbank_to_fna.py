#!/usr/bin/env python3
"""Convert a GenBank file into GFF format.

Usage:
    genbank_to_gff.py <genbank_file>
"""
import sys
import os

from Bio import SeqIO
from Bio import Seq

def main(gb_file):
    input_handle  = open(gb_file, "r")
    output_handle = open("%s.fna" % os.path.splitext(gb_file)[0], "w")
    for seq_record in SeqIO.parse(input_handle, "genbank") :
#        print "Dealing with GenBank record %s" % seq_record.id
        output_handle.write(">{} {}\n{}\n".format(seq_record.id, seq_record.description, str(seq_record.seq)))
    input_handle.close()
    output_handle.close()

#    out_file = "%s.fna" % os.path.splitext(gb_file)[0]
#    with open(out_file, "w") as out_handle:
#        GFF.write(SeqIO.parse(gb_file, "genbank"), out_handle)

if __name__ == "__main__":
    main(*sys.argv[1:])

#Short version:
#SeqIO.write(SeqIO.parse(input_handle, "genbank"), output_handle, "fasta")
