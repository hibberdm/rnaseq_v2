#!/usr/bin/env python3

"""Convert a GenBank file into FFN format.

Usage:
    genbank_to_ffn.py <genbank_file>
"""
import sys
import os

from Bio import SeqIO
from Bio import Seq

def main(gb_file):
    input_handle  = open(gb_file, "r")
    output_handle = open("%s.faa" % os.path.splitext(gb_file)[0], "w")
    for seq_record in SeqIO.parse(input_handle, "genbank") :
#        print "Dealing with GenBank record %s" % seq_record.id
        for seq_feature in seq_record.features:
            if seq_feature.type=="CDS":
                assert len(seq_feature.qualifiers['gene']) == 1
                assert len(seq_feature.qualifiers['translation']) == 1
                assert len(seq_feature.qualifiers['product']) == 1
                output_handle.write(">{} {}\n{}\n".format(seq_feature.qualifiers['gene'][0], seq_feature.qualifiers['product'][0], seq_feature.qualifiers['translation'][0]))
    input_handle.close()
    output_handle.close()

#    out_file = "%s.fna" % os.path.splitext(gb_file)[0]
#    with open(out_file, "w") as out_handle:
#        GFF.write(SeqIO.parse(gb_file, "genbank"), out_handle)

if __name__ == "__main__":
    main(*sys.argv[1:])

#Short version:
#SeqIO.write(SeqIO.parse(input_handle, "genbank"), output_handle, "fasta")
