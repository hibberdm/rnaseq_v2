#!/bin/bash

#SBATCH --mem=2G

set -e

WORK_DIR=$( pwd )
export WORK_DIR
export LOG_DIR=${WORK_DIR}/logs

mkdir -p "${LOG_DIR}"

# VARIABLES #
MAPPING_FILE="mapping_file.txt"
GENOMES_DIR="genomes"
TRIMMED_READ_LENGTH="50"
STRANDED="1"

sed -i -e '$a\' "$MAPPING_FILE"

### NEED NUMBER OF UNIQUE SAMPLES POST-CAT for cat.sbatch
# consider bowtie2
# is fastx the best read trimmer
# work out gb -> fna/gff conversion strategy

ml python/3.6.5
ml py-biopython/1.73-python-3.6.5
ml bowtie/1.2.2
ml fastx_toolkit/0.0.13

PATH=/home/hibberdm/rnaseq_v2/bin:$PATH

#1 Prep databases from various genome combinations
#2 Find and concatenate (if necessary) data files
#3 Map reads to databases
#4 Transcript counting with featureCounts or RSEM
#5 Output aggregation and normalization (TPM?)

rnaseq_v2_pipeline_prep.py -m "$MAPPING_FILE" -g "$GENOMES_DIR"

PREPROCESSING_TASKS=$( sed -ne '/^INDEX/!p' "$MAPPING_FILE" | wc -l )
SAMPLES=$( sed -ne '/^INDEX/!p' "$MAPPING_FILE" | cut -f 2 | sort | uniq | wc -l )
COUNTING_TASKS=$( wc -l "counting_tasks.job" | cut -d " " -f 1 )

J1=$( sbatch -o "${LOG_DIR}/slurm-trim-%A_%a.out" --array=1-"$PREPROCESSING_TASKS"%100 trim.sbatch "$MAPPING_FILE" "$TRIMMED_READ_LENGTH" | awk '{ print $NF }' )
J2=$( sbatch -o "${LOG_DIR}/slurm-cat-%A_%a.out" --array=1-"$SAMPLES"%100 -d afterok:"${J1}" cat.sbatch | awk '{ print $NF }' )
J3=$( sbatch -o "${LOG_DIR}/slurm-bowtie-mapping-%A_%a.out" --array=1-"$SAMPLES"%100 -d aftercorr:"${J2}" bowtie.sbatch | awk '{ print $NF }' )
J4=$( sbatch -o "${LOG_DIR}/slurm-counting-featurecounts-%A_%a.out" --array=1-"$COUNTING_TASKS"%100 -d afterok:"${J3}" counting_featurecounts.sbatch "$STRANDED"| awk '{ print $NF }' ) 
