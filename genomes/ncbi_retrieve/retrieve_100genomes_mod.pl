#!/usr/bin/env perl

#Want to query NCBI a list of genomes (100G community)
#Developed script by editing things posted at https://www.biostars.org/p/10959/ and 
#http://www.ncbi.nlm.nih.gov/books/NBK25498/#chapter3.Application_4_Finding_unique_se
#Usage perl retrieve_100genomes.pl <accession file> <output file>

use strict;
use warnings;

use LWP::Simple;
#use LWP::UserAgent;
#my $db1 = 'nuccore';

#my @gg_ids;
#my @ids;
#my @header;
#my $output;

open(IN, $ARGV[0])
or die "Input file cannot be opened";

my $header = <IN>;

while(<IN>){
	chomp($_);
	my @line=split(/\t/,$_);
	print $line[3],"\t";
	print $line[2],"\n";
	if( $line[3]=~/g$/){
	    get_sequence_g($line[2], $line[0]);
	}
	elsif($line[3]=~/gp$/){
	    get_sequence_g($line[2], $line[0]);
	}
	elsif($line[3]=~/s$/){
	    get_sequence_s($line[2], $line[0]);
	}
	elsif($line[3]=~/q$/){
	    get_sequence_q($line[2], $line[0]);
	}
}

sub get_sequence_g {

    #Get id
    my $id=$_[0];
    chomp($id);
    my $local_name=$_[1];
    chomp($local_name);
    #assemble the esearch URL
    my $base = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/';
    my $url = $base . "efetch.fcgi?";

    my $url_params = $url . "db=nuccore&rettype=gbwithparts&retmode=text";
    $url_params .= "&id=$id";

    print $url_params;

    my $output = get($url_params);
    open (OUT, '>',"$local_name.gb") || die "Can't open output file!\n";
    print OUT "$output";
    close OUT;
}

sub get_sequence_s {

    #Get id
    my $input=$_[0];
    my @range=split(/:/,$input);
    my $local_name=$_[1];
    chomp($local_name);
    #my $prefix;
    #my $start;
    #my $prefix2;
    #my $end;
    my ($prefix, $start) =$range[0] =~ /(\D+)(\d+)/;
    my ($prefix2, $end) = $range[1] =~ /(\D+)(\d+)/;
    print "$input\t$prefix\t$prefix2\t$start\t$end\n";
    my $id;
    if ($prefix =~ $prefix2){
	my @nums = ($start..$end);
	$id = join(",", map {$prefix . $_} @nums);
    }
    else {die "Input file cannot be opened";}    
    
    #assemble the esearch URL
    my $base = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/';
    my $url = $base . "efetch.fcgi?";

    my $url_params = $url . "db=nuccore&rettype=gbwithparts&retmode=text";
    $url_params .= "&id=$id";

    print $url_params;

    my $output = get($url_params);
    open (OUT, '>',"$local_name.gb") || die "Can't open output file!\n";
    print OUT "$output";
    close OUT;
}
sub get_sequence_q {

    #Get id
    my $input_or=$_[0];
    my $local_name=$_[1];
    chomp($local_name);
  #  print $input_or;
    my @input_split = split(/OR/,$input_or);
   # print $input_split[0];
   # print $input_split[1];
    my @file = $input_split[0] =~ /\(((\D+)(\d+))/;
 #   print "File is $file[0]\n";
    open (OUT, '>',"$local_name.gb") || die "Can't open output file!\n";
       
    while (@input_split){
	my $input = shift @input_split;
	chomp($input);
	my @range=split(/:/,$input);
#	print "blah$input\n$range[0]\t$range[1]";
	my ($prefix, $start) = $range[0] =~ /\((\D+)(\d+)/;
	my ($prefix2, $end) = $range[1] =~ /(\D+)(\d+)/;
	print "$input\t$prefix\t$prefix2\t$start\t$end\n";
	my $id;
	if ($prefix =~ $prefix2){
	    my @nums = ($start..$end);
	    $id = join(",", map {$prefix . $_} @nums);
	}
	else {die "Input file cannot be opened";}

	#assemble the esearch URL
	my $base = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/';
	my $url = $base . "efetch.fcgi?";

	my $url_params = $url . "db=nuccore&rettype=gbwithparts&retmode=text";
	$url_params .= "&id=$id";

	print $url_params;

	my $output = get($url_params);
	print OUT "$output";
    }
  close OUT;
}
