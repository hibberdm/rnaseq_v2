#!/usr/bin/env python3

import sys
import os
import errno
import argparse
import hashlib
from Bio import SeqIO
import subprocess
import shutil

parser = argparse.ArgumentParser()
parser.add_argument("-m", help="[STRING] Input mapping file", dest="mapfile", required=True)
parser.add_argument("-g", help="[STRING] Genome directory", dest="genome_dir", required=True)
parser.add_argument("-r", help="[STRING] Set up pipeline for RSEM counting", dest="rsem_flag", action="store_true")
args = parser.parse_args()

command = 'bowtie-build'
if not (shutil.which(command) is not None):
	exit("ERROR: " + command + " is not available!")

#genome_ext = ['.fna']
#genome_files = [fn for fn in os.listdir(args.genome_dir)
#    if any(fn.endswith(ext) for ext in genome_ext)]

mapped_dir = "03_mapped/"
mapping_file = open( args.mapfile, 'r')
mapping_lines = mapping_file.readlines()
first = True
headers = list()
databases = dict()
mapping_jobs = set()
counting_jobs = dict()
suffixes = set()
for line in mapping_lines:
	if line.startswith('#'):
		continue
	else:
		stripline = line.strip()
		splitline = stripline.split('\t')
		if (first):
			headers = splitline
			first = False
		else:
			sample_id = splitline[headers.index('SID')]
			genome_set = splitline[headers.index('GENOMES')]
			genome_set_sort = sorted(genome_set.split(','))
			db_name = ','.join(genome_set_sort)
			db_hash = hashlib.md5(db_name.encode('utf-8')).hexdigest()
			for genome in genome_set_sort:
				suffixes.add(os.path.splitext(genome)[1])
#				sys.stdout.write("Genome: {}".format(genome))
				if not (os.path.isfile(os.path.join(args.genome_dir, genome))):
					sys.exit("[ERROR] Genome not found: " + genome)
				if not args.rsem_flag:
					if not genome in counting_jobs:
						counting_jobs[genome] = set()
						counting_jobs[genome].add(mapped_dir + sample_id + ".bam")
					else:
						counting_jobs[genome].add(mapped_dir + sample_id + ".bam")
			if not (genome_set in databases):
				#                print("{}\n".format(db_name))
				databases[genome_set] = db_hash
				if len(suffixes) > 1:
					sys.exit("[ERROR] Multiple genome suffixes found - do you have consistent file types?: " + ",".join(suffixes))
				else:
					db_path = os.path.join(args.genome_dir, db_hash + list(suffixes)[0])
#				print(db_path)
				if not (os.path.isfile(db_path)):
					sys.stdout.write("[STATUS] Creating database: " + db_path + "\n")
					sys.stdout.flush()
					handle = open(db_path, "a")
					for genome in genome_set_sort:
						#                    print("{}\n".format(genome))
						for record in SeqIO.parse(os.path.join(args.genome_dir, genome), "fasta"):
							record.description += " | " + os.path.splitext(genome)[0]
							SeqIO.write(record, handle, "fasta")
					handle.close()
				else:
					sys.stdout.write("[STATUS] Existing database for " + db_path + " found!\n")
					sys.stdout.flush()
				if not (os.path.isfile(db_path + ".1.ebwt")):
					sys.stdout.write("[STATUS] Creating bowtie index for " + db_path + "\n")
					sys.stdout.flush()
					subprocess.call(['bowtie-build', '-q', db_path, db_path])
			mapping_jobs.add(sample_id + "\t" + db_hash + list(suffixes)[0] + "\t" + db_name + "\n")
			if args.rsem_flag:
				counting_jobs[sample_id + ".bam"] = db_hash + list(suffixes)[0]

mapping_jobs_file = open("mapping_tasks.job", 'w')
counting_jobs_file = open("counting_tasks.job", 'w')

for mapping_job in sorted(mapping_jobs):
	mapping_jobs_file.write(mapping_job)

for genome in counting_jobs:
#	sys.stdout.write(" ".join(counting_jobs[genome]))
	if args.rsem_flag:
		counting_jobs_file.write("{}\t{}\n".format(counting_jobs[genome], genome))
	else:
		counting_jobs_file.write("{}\t{}\n".format(genome, " ".join(counting_jobs[genome])))

mapping_jobs_file.close()
counting_jobs_file.close()
