#!/usr/bin/env python3

import sys
import os
import errno
import argparse
import hashlib
from Bio import SeqIO
import subprocess
import shutil

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--mapping", help="[STRING] Input mapping file", dest="mapfile", required=True)
parser.add_argument("-g", "--genomes_dir", help="[STRING] Genome directory", dest="genome_dir", required=True)
args = parser.parse_args()

command = 'bowtie-build'
if not (shutil.which(command) is not None):
	exit("ERROR: " + command + " is not available!")

genome_ext = ['.fna']
genome_files = [fn for fn in os.listdir(args.genome_dir)
    if any(fn.endswith(ext) for ext in genome_ext)]

mapping_file = open( args.mapfile, 'r')
mapping_lines = mapping_file.readlines()
jobs_file = open( "mapping_jobs.txt", 'w')
first = True
headers = list()
databases = dict()
jobs = set()
for line in mapping_lines:
	if line.startswith('#'):
		continue
	else:
		stripline = line.strip()
		splitline = stripline.split('\t')
		if (first):
			headers = splitline
			first = False
		else:
			sample_id = splitline[headers.index('SID')]
			genome_set = splitline[headers.index('GENOMES')]
			genome_set_sort = sorted(genome_set.split(','))
			for genome in genome_set_sort:
#				sys.stdout.write("Genome: {}".format(genome))
				if not (os.path.isfile(os.path.join(args.genome_dir, genome))):
					sys.exit("[ERROR] Genome not found: " + genome)
			db_name = ','.join(genome_set_sort)
			if not (genome_set in databases):
				db_hash = hashlib.md5(db_name.encode('utf-8')).hexdigest()
				#                print("{}\n".format(db_name))
				databases[genome_set] = db_hash
				db_path = os.path.join(args.genome_dir, db_hash + ".fna")
#				print(db_path)
				if not (os.path.isfile(db_path)):
					print("[STATUS] Creating database: " + db_path)
					handle = open(db_path, "a")
					for genome in genome_set_sort:
						#                    print("{}\n".format(genome))
						for record in SeqIO.parse(os.path.join(args.genome_dir, genome), "fasta"):
							record.description += " | " + os.path.splitext(genome)[0]
							SeqIO.write(record, handle, "fasta")
					handle.close()
				else:
					print("[STATUS] Existing database for " + db_path + " found!")
				if not (os.path.isfile(db_path + ".1.ebwt")):
					print("[STATUS] Creating bowtie index for " + db_path)
					subprocess.call(['bowtie-build', '-q', db_path, db_path])
			jobs.add(sample_id + "\t" + db_path + "\n")

for job in sorted(jobs):
	jobs_file.write(job)
