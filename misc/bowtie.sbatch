#!/bin/bash

#SBATCH --mem=30G
#SBATCH --cpus-per-task=8

set -e

. vars.sh

if [ -z "${1}" ]; then
    echo "Must pass the mapping file"
    exit 1
fi

ml bowtie/1.2.2 
ml samtools/1.6

MAP_LINE=$( sed -ne '/^#/!p' ${1} | sed -n ${SLURM_ARRAY_TASK_ID}p )
IFS=$'\t' read -r INDEX SID genomes <<< "$MAP_LINE"
unset IFS

DB_LINE=$( sed -n ${SLURM_ARRAY_TASK_ID}p "mapping_jobs.txt")
IFS=$'\t' read -r INDEX db_name <<< "$DB_LINE"
unset IFS

if [ ! -f "${db_name}" ]
then
	echo "ERROR: Database $db_name not found in ${GENOMES_DIR}!"
	exit 1
fi

bowtie \
 -S \
 -a \
 --best \
 --strata \
 -q \
 -p ${SLURM_CPUS_PER_TASK} \
 -v 1 \
 $db_name \
 ${DEMUX_DIR}/${SID}_R1.fastq \
 | samtools view -hbS | samtools sort - -o ${MAPPED_DIR}/${SID}_R1.bam

