#!/usr/bin/python3

import sys
import os
import errno
import argparse
import hashlib
from Bio import SeqIO
import subprocess
import shutil

parser = argparse.ArgumentParser()
parser.add_argument("-m", help="[STRING] Input mapping file", dest="mapfile", required=True)
parser.add_argument("-d", help="[STRING] Input data directory", dest="data_dir", default="01_input_data")
parser.add_argument("-t", help="[INTEGER] Trimmed read length", dest="trim_length", type=int)
args = parser.parse_args()

command = 'fastx_trimmer'
if not (shutil.which(command) is not None):
	exit("ERROR: " + command + " is not available!")

preprocessed_dir = "02_preprocessed_data"
if not os.path.exists(preprocessed_dir):
        os.mkdir(preprocessed_dir)

mapping_file = open( args.mapfile, 'r')
mapping_lines = mapping_file.readlines()
first = True
sample_tracker = dict()
for line in mapping_lines:
	if line.startswith('#'):
		continue
	else:
		stripline = line.strip()
		splitline = stripline.split('\t')
		if (first):
			headers = splitline
			first = False
		else:
			sample_id = splitline[headers.index('SID')]
			seq_data = splitline[headers.index('SEQ_DATA')]
			if not sample_id in sample_tracker:
				sample_tracker[sample_id] = set()
			if (args.trim_length):
				seq_data_trim = os.path.splitext(os.path.basename(seq_data))[0] + "_trim" + str(args.trim_length) + os.path.splitext(os.path.basename(seq_data))[1]
				subprocess.call(['fastx_trimmer', '-l', str(args.trim_length), '-i', os.path.join(args.data_dir, seq_data), '-o', os.path.join(preprocessed_dir, seq_data_trim), '-Q33'])
				sample_tracker[sample_id].add(os.path.join(preprocessed_dir, seq_data_trim))
			else:
				sample_tracker[sample_id].add(os.path.join(args.data_dir, seq_data))

for sample_id in sample_tracker:
	if length(sample_tracker[sample_id]) > 1:
		seq_out = sample_id + os.path.splitext(sample_tracker[sample_id][0])[0]
		subprocess.call(['cat', '\s'.join(sample_tracker[sample_id]), '>', seq_out])
	else:
		seq_out = sample_id + os.path.splitext(sample_tracker[sample_id])[0]
		subprocess.call(['ln', '-s', os.path.join(args.data_dir, sample_tracker[sample_id]), os.path.join(preprocess_dir, seq_out)])
