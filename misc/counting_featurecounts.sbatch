#!/bin/bash

#SBATCH --mem=4G
#SBATCH --cpus-per-task=8

set -e

WORK_DIR=$( pwd )
export WORK_DIR
export GENOMES_DIR=${WORK_DIR}/genomes
export MAPPED_DIR=${WORK_DIR}/03_mapped
export COUNTS_DIR=${WORK_DIR}/04_counts

#if [ -z "${1}" ]; then
#    echo "Must pass the metadata"
#    exit 1
#fi

mkdir -p "$COUNTS_DIR"

DB_LINE=$( sed -n ${SLURM_ARRAY_TASK_ID}p "mapping_tasks.job")
IFS=$'\t' read -r SID DB GENOMES <<< "$DB_LINE"
unset IFS

if [ "${1}" = "1" ]; then
 STRANDED=1
else
 STRANDED=0
fi

ml samtools/1.9
ml subread/1.6.2

DB_BASE=${DB%.*}

## Community-level counting ##
COMMUNITY_GFF_BASE=${DB%.*}

featureCounts \
 ${MAPPED_DIR}/${SID}_${DB_BASE}.bam \
 -a ${GENOMES_DIR}/${COMMUNITY_GFF_BASE}.gff \
 -t CDS \
 -g locus_tag \
 -o ${COUNTS_DIR}/${SID}_${DB_BASE}.counts \
 -s $STRANDED \
 -T $SLURM_CPUS_PER_TASK

## Organism-level counting ##
IFS=',' read -r -a GENOME_ARRAY <<< "$GENOMES"
unset IFS
for GENOME in "${GENOME_ARRAY[@]}"
do
 GENOME_GFF_BASE=${GENOME%.*}
 featureCounts \
 ${MAPPED_DIR}/${SID}_${DB_BASE}.bam \
 -a ${GENOMES_DIR}/${GENOME_GFF_BASE}.gff \
 -t CDS \
 -g locus_tag \
 -o ${COUNTS_DIR}/${SID}_${GENOME_GFF_BASE}.counts \
 -s $STRANDED \
 -T $SLURM_CPUS_PER_TASK
done
