export WORK_DIR=/home/hibberdm/scratch/rnaseq/180820_v2
export DEMUX_DIR=${WORK_DIR}/demux
export GENOMES_DIR=${WORK_DIR}/genomes
export MAPPED_DIR=${WORK_DIR}/mapped
export LOG_DIR=${WORK_DIR}/logs

